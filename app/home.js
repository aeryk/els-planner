'use client'

import Image from 'next/image'
import { useEffect, useReducer, useState } from 'react'
import Badge from 'react-bootstrap/Badge'

import CharactersDisplay from './components/CharactersDisplay'
import PageBase from './components/PageBase'
import RootNav from './components/RootNav'
import GameAccount from './models/game/GameAccount'

import styles from './home.module.css'
import SpanInput from './components/SpanInput'

function CharacterCell ({ src, ign }) {
  return (
    <div className={'mx-2 rounded-5 overflow-hidden ' + styles['cell']}>
      <Image src={src} width={170} height={170} alt={ign} />
      <p className='mb-0'>{ign}</p>
    </div>
  )
}

function reducer (state, action) {
  switch (action.type) {
    case 'replace':
      return {
        ...state,
        ...action.account
      }
    case 'rename':
      return {
        ...state,
        name: action.name
      }
    case 'set_erp':
      return {
        ...state,
        erp: action.erp
      }
    case 'set_accs': {
      return {
        ...state,
        accessories: {
          ...state.accessories,
          ...action.accessories
        }
      }
    }
  }
}



export default function Home () {
  const [ account, modifyAccount ] = useReducer(reducer, {})
  const [ loaded, setLoaded ] = useState(false)

  useEffect(() => {
    const raw = localStorage.getItem('account')
    const account = raw ? JSON.parse(raw) : new GameAccount('Main')
    modifyAccount({
      type: 'replace',
      account
    })
    setLoaded(true)
  }, [])

  useEffect(() => {
    localStorage.setItem('account', JSON.stringify(account))
  }, [account])

  const updateErp = (newErp) => {
    const action = {
      type:'set_erp',
      erp: newErp
    }
    modifyAccount(action)
  }

  return (
    <>
      <RootNav />
      <PageBase>
        <Badge pill bg='secondary'>
          ERP{' '}{loaded ?(
            <SpanInput
              type='number'
              inputMode='numeric'
              min={0}
              max={999}
              step={1}
              onChange={(e) => updateErp(e.target.value)}
            >
              {account.erp}
            </SpanInput>
          ) : 0}
        </Badge>
        <CharactersDisplay />
        <h1 className='my-4'>Grid Layout Sample</h1>
        <CharacterCell src='/img-2/char/eve/cs.png' ign='ElectricTail' />
        <CharacterCell src='/img-2/char/aisha/la.png' ign='Cagliostro' />
        <CharacterCell src='/img-2/char/ara/aps.png' ign='SakuraChiyo' />
        <br className='mb-3' />
        <CharacterCell src='/img-2/char/eve/csm.png' ign='ElectricTail' />
        <CharacterCell src='/img-2/char/aisha/lam.png' ign='Cagliostro' />
        <CharacterCell src='/img-2/char/ara/apsm.png' ign='SakuraChiyo' />
      </PageBase>
    </>
  )
}
