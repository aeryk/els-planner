'use client'

import PageBase from '@/components/PageBase'
import RootNav from '@/components/RootNav'
import WeaponTypes from '@/models/weapon/WeaponTypes'
import SpanInput from '@/components/SpanInput'
import SpanDropdown from '@/components/SpanDropdown'

export default function Home () {
  return (
    <>
      <RootNav />
      <PageBase>
        <h1>Sandbox</h1>
        <h3>
          {'+'}
          <SpanInput
            type='number'
            inputMode='numeric'
            min={0}
            max={13}
            step={1}
          >
            11
          </SpanInput>
          {' '}
          <SpanDropdown
            options={Object.values(WeaponTypes)}
            optionValue='shortName'
            optionLabel='longName'
            initialIndex={1} />
        </h3>
      </PageBase>
    </>
  )
}
