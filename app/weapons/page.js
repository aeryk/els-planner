'use client'

import Weapons from '@/components/Weapons'
import PageBase from '@/components/PageBase'
import RootNav from '@/components/RootNav'

export default function Home () {
  return (
    <>
      <RootNav />
      <PageBase>
        <Weapons />
      </PageBase>
    </>
  )
}
