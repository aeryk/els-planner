// from: https://elwiki.net/w/Module:ColorSel/data
const Characters = {
  Elsword: '#db2f2f',
  Aisha: '#9400d3',
  Rena: '#41d941',
  Raven: '#333',
  Eve: '#ff93ae',
  Chung: '#9cf',
  Ara: '#ef8d2d',
  Elesis: '#9b111e',
  Add: '#9f81f7',
  LuCiel: '#1953b4',
  // Lu: '#4089f8',
  // Ciel: '#1824f0',
  Rose: '#e9c92c',
  Ain: '#19d2a8',
  Laby: '#db4183',
  Nisha: '#4f55a7',
  Noah: '#414482',
  Lithia: '#3aa370'
}

export default ({
  Characters
})
