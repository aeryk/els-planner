import './global.css'

import { Noto_Sans } from 'next/font/google'

import Script from 'next/script'

const notoSans = Noto_Sans({
  subsets: ['latin'],
  variable: '--font-noto-sans',
  display: 'swap'
})

/** @type {import('next').Metadata} */
export const metadata = {
  title: 'Els Planner',
  icons: ['/img/questIcon2.png']
}

/** @type {import('next').Viewport} */
export const viewport = {
  colorScheme: 'dark'
}

export default function RootLayout ({ children }) {
  return (
    <html lang='en' className={notoSans.variable} data-bs-theme='auto'>
      <head>
        <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css'
          rel='stylesheet'
          integrity='sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH'
          crossOrigin='anonymous' />
      </head>
      <body>
        {children}
        <Script src='/color-modes.js' />
      </body>
    </html>
  )
}
