export class EquipmentSlot {
  static Weapon = new EquipmentSlot('weapon')

  static Top = new EquipmentSlot('top')
  static Bottom = new EquipmentSlot('bottom')
  static Gloves = new EquipmentSlot('gloves')
  static Shoes = new EquipmentSlot('shoes')

  static ArtifactCirclet = new EquipmentSlot('arti-circlet')
  static ArtifactArmor = new EquipmentSlot('arti-armor')
  static ArtifactNecklace = new EquipmentSlot('arti-necklace')
  static ArtifactRing = new EquipmentSlot('artif-ring')

  constructor (name) {
    /** @type {string} */
    this.name = name
  }
}

/**
 * Shared class for Weapon and Armor
 */
export default class Equipment {
  /** @type {EquipmentSlot} */
  slot
  // TODO: Move these three to just armor and weapon?
  enhancement // maybe not this since game has +0 on non-armor/wep?
  // or just stop matching the game and take it out from here
  // attribute
  wedged
  
  /**
   * - Elite `3`
   * - Unique `4`
   * - Legend `5`
   * @type {3|4|5}
   */
  rarity = 4
  
  /** @type {import('./Socket')[]} */
  sockets = []

  constructor (slot) {
    if (slot instanceof EquipmentSlot)
      this.slot = slot
    else
      throw new Error('Invalid Equipment slot ' + slot)

    this.enhancement = 0
    this.wedged = false
  }

  /**
   * Sets the enhancement value of the equipment.
   * @param {number} value - The enhancement value to set
   */
  setEnhancement (value) {
    if (value < 0 || value > 13)
      throw new Error('Invalid enhance value: ' + value)

    this.enhancement = value
  }

  /**
   * Applies the wedge on the equipment
   * @param {boolean} [wedged] - new value. true if unspecified
   */
  setWedged (wedged) {
    this.wedged = wedged ?? true
  }
}