import Weapon from '@/models/weapon/Weapon'
import CharacterClass from './CharacterClass'
import Characters, { AllChars } from './Characters'
import ElsClasses from './ElsClasses'

export default class GameCharacter {
  /** @type {string} */
  ign

  /** @type {number} */
  level

  /** @type {string} */
  char

  /** @type {CharacterClass} */
  eClass

  /** @type {Weapon[]} */
  weapons

  /** @type {{[name: string]: boolean}} */
  accessories

  /** @type {{}} */
  titles

  /** @type {{}} */
  artifact

  /** @type {{}} */
  forceSkills

  /* Other per-char info:

  Customization
  ERP Build (can be generalized to account wide)
  Skill Builds

  */

  /**
   * @param {string} ign In-game Name
   * @param {number} level Character Level
   * @param {Characters|string} char Game Character
   * @param {CharacterClass|string} clas Character Class
   */
  constructor (ign, level, char, clas) {
    this.setIgn(ign)
    this.setLevel(level)
    
    if (char instanceof Characters)
      this.char = char.name
    else if (typeof char === 'string' && AllChars[char.toLowerCase()] != undefined)
      this.char = char.toLowerCase()
    else
      throw new Error('[Char Con] Invalid Character ' + char)

    if (clas instanceof CharacterClass)
      this.eClass = clas.short
    else if (ElsClasses.isValidByShort(clas) || ElsClasses.isValidByName(clas))
      this.eClass = clas
    else
      throw new Error('[Char Con] Invalid Character Class ' + clas)

    this.weapons = []
    this.accessories = {}
    this.titles = {}
    this.artifact = {}
    this.forceSkills = {}
  }

  /**
   * Sets in-game name, given the length constraints
   * @param {string} ign In-game Name
   */
  setIgn (ign) {
    if (ign.length >= 2 && ign.length <= 12)
      this.ign = ign
    else
      throw new Error('Invalid IGN \'' + ign + '\' (length ' + ign.length + ')')
  }

  /**
   * Sets level
   * @param {number} level Character level
   */
  setLevel (level) {
    if (level > 0 && level < 100)
      this.level = level
    else
      throw new Error('Invalid level: ' + level)
  }

  /**
   * @param {CharacterClass} clas New Class
   */
  jobChange (clas) {
    if (clas.char === this.char)
      this.clas = clas
  }

  /**
   * Adds a weapon
   * @param {import("../weapon/WeaponType")} type Weapon Type
   * @returns {Weapon} Generated Weapon
   */
  addWeapon (type) {
    this.weapons.push(new Weapon(type, this.char))
    return this.weapons[this.weapons.length - 1]
  }
}