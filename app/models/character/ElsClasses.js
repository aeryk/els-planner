import Characters from './Characters'
import CharacterClass from './CharacterClass'

/**
 * @type {{[char: string]: {[short: string]: CharacterClass}}}
 */
const ClassMap = {}

const Names = []
const ShortNames = []

/**
 * @param {Characters} char Game Character
 * @param {string} name Character Class Name
 * @param {string} short Class shorthand
 * @returns {CharacterClass} Game Class instance
 */
function elsClass (char, name, short) {
  const out = new CharacterClass(char, name, short)
  Names.push(out.name)
  ShortNames.push(out.short)
  ClassMap[out.char] ??= {}
  ClassMap[out.char][out.short] = out
  return out
}

class ElsClasses {
  static KnightEmperor = elsClass(Characters.Elsword, 'Knight Emperor', 'ke')
  static KE = this.KnightEmperor

  static RuneMaster = elsClass(Characters.Elsword, 'Rune Master', 'rm')
  static RM = this.RuneMaster

  static Immortal = elsClass(Characters.Elsword, 'Immortal', 'im')
  static IM = this.Immortal

  static Genesis = elsClass(Characters.Elsword, 'Genesis', 'gs')
  static GS = this.Genesis

  static AetherSage = elsClass(Characters.Aisha, 'Aether Sage', 'aes')
  static AeS = this.AetherSage

  static OzSorcerer = elsClass(Characters.Aisha, 'Oz Sorcerer', 'os')
  static OS = this.OzSorcerer

  static Metamorphy = elsClass(Characters.Aisha, 'Metamorphy', 'mtm')
  static Mtm = this.Metamorphy

  static LordAzoth = elsClass(Characters.Aisha, 'Lord Azoth', 'la')
  static LA = this.LordAzoth

  static Daybreaker = elsClass(Characters.Rena, 'Daybreaker', 'dab')
  static DaB = this.Daybreaker

  static Anemos = elsClass(Characters.Rena, 'Anemos', 'an')
  static AN = this.Anemos

  static Twilight = elsClass(Characters.Rena, 'Twilight', 'tw')
  static TW = this.Twilight

  static Prophetess = elsClass(Characters.Rena, 'Prophetess', 'pr')
  static PR = this.Prophetess

  static RageHearts = elsClass(Characters.Raven, 'Rage Hearts', 'rh')
  static RH = this.RageHearts

  static FuriousBlade = elsClass(Characters.Raven, 'Furious Blade', 'fb')
  static FB = this.FuriousBlade

  static NovaImperator = elsClass(Characters.Raven, 'Nova Imperator', 'ni')
  static NI = this.NovaImperator

  static Revenant = elsClass(Characters.Raven, 'Revenant', 'rv')
  static RV = this.Revenant

  static CodeUltimate = elsClass(Characters.Eve, 'Code: Ultimate', 'cu')
  static CU = this.CodeUltimate

  static CodeEsencia = elsClass(Characters.Eve, 'Code: Esencia', 'ce')
  static CE = this.CodeEsencia

  static CodeSariel = elsClass(Characters.Eve, 'Code: Sariel', 'cs')
  static CS = this.CodeSariel

  static CodeAntithese = elsClass(Characters.Eve, 'Code: Antithese', 'ca')
  static CA = this.CodeAntithese

  static CometCrusader = elsClass(Characters.Chung, 'Comet Crusader', 'cc')
  static CC = this.CometCrusader

  static FatalPhantom = elsClass(Characters.Chung, 'Fatal Phantom', 'fp')
  static FP = this.FatalPhantom

  static Centurion = elsClass(Characters.Chung, 'Centurion', 'cet')
  static Cet = this.Centurion

  static DiusAer = elsClass(Characters.Chung, 'Dius Aer', 'da')
  static DA = this.DiusAer

  static Apsara = elsClass(Characters.Ara, 'Apsara', 'aps')
  static Aps = this.Apsara

  static Devi = elsClass(Characters.Ara, 'Devi', 'de')
  static De = this.Devi

  static Shakti = elsClass(Characters.Ara, 'Shakti', 'sh')
  static SH = this.Shakti

  static Surya = elsClass(Characters.Ara, 'Surya', 'su')
  static SU = this.Surya

  static EmpireSword = elsClass(Characters.Elesis, 'Empire Sword', 'es')
  static ES = this.EmpireSword

  static FlameLord = elsClass(Characters.Elesis, 'Flame Lord', 'fl')
  static FL = this.FlameLord

  static BloodyQueen = elsClass(Characters.Elesis, 'Bloody Queen', 'bq')
  static BQ = this.BloodyQueen

  static Adrestia = elsClass(Characters.Elesis, 'Adrestia', 'ad')
  static AD = this.Adrestia

  static DoomBringer = elsClass(Characters.Add, 'Doom Bringer', 'db')
  static DB = this.DoomBringer

  static Dominator = elsClass(Characters.Add, 'Dominator', 'dom')
  static Dom = this.Dominator

  static MadParadox = elsClass(Characters.Add, 'Mad Paradox', 'mp')
  static MP = this.MadParadox

  static Overmind = elsClass(Characters.Add, 'Overmind', 'om')
  static OM = this.Overmind

  static Catastrophe = elsClass(Characters.LuCiel, 'Catastrophe', 'cat')
  static Cat = this.Catastrophe

  static Innocent = elsClass(Characters.LuCiel, 'Innocent', 'in')
  static In = this.Innocent

  static Diangelion = elsClass(Characters.LuCiel, 'Diangelion', 'dia')
  static Dia = this.Diangelion

  static Demersio = elsClass(Characters.LuCiel, 'Demersio', 'dem')
  static Dem = this.Demersio

  static TempestBurster = elsClass(Characters.Rose, 'Tempest Burster', 'tb')
  static TB = this.TempestBurster

  static BlackMassacre = elsClass(Characters.Rose, 'Black Massacre', 'bkm')
  static BkM = this.BlackMassacre

  static Minerva = elsClass(Characters.Rose, 'Minerva', 'min')
  static MN = this.Minerva

  static PrimeOperator = elsClass(Characters.Rose, 'Prime Operator', 'po')
  static PO = this.PrimeOperator

  static Richter = elsClass(Characters.Ain, 'Richter', 'rt')
  static RT = this.Richter

  static Bluhen = elsClass(Characters.Ain, 'Bluhen', 'bl')
  static BL = this.Bluhen

  static Herrscher = elsClass(Characters.Ain, 'Herrscher', 'hr')
  static HR = this.Herrscher

  static Opferung = elsClass(Characters.Ain, 'Opferung', 'op')
  static OP = this.Opferung

  static EternityWinner = elsClass(Characters.Laby, 'Eternity Winner', 'etw')
  static EtW = this.EternityWinner

  static RadiantSoul = elsClass(Characters.Laby, 'Radiant Soul', 'ras')
  static RaS = this.RadiantSoul

  static NishaLabyrinth = elsClass(Characters.Laby, 'Nisha Labyrinth', 'nl')
  static NL = this.NishaLabyrinth

  static TwinsPicaro = elsClass(Characters.Laby, 'Twins Picaro', 'tp')
  static TP = this.TwinsPicaro

  static Liberator = elsClass(Characters.Noah, 'Liberator', 'lib')
  static Lib = this.Liberator

  static Celestia = elsClass(Characters.Noah, 'Celestia', 'cl')
  static CL = this.Celestia

  static NyxPieta = elsClass(Characters.Noah, 'Nyx Pieta', 'np')
  static NP = this.NyxPieta

  static Morpheus = elsClass(Characters.Noah, 'Morpheus', 'mo')
  static MO = this.Morpheus

  static Gembliss = elsClass(Characters.Lithia, 'Gembliss', 'gb')
  static GB = this.Gembliss

  static Avarice = elsClass(Characters.Lithia, 'Avarice', 'av')
  static AV = this.Avarice

  static Achlys = elsClass(Characters.Lithia, 'Achlys', 'ac')
  static AC = this.Achlys

  static Mischief = elsClass(Characters.Lithia, 'Mischief', 'mc')
  static MC = this.Mischief

  /**
   * @param {string} char Game Character name
   * @returns {CharacterClass[]} Classes of character
   */
  static namesByCharacter (char) {
    if (char in ClassMap)
      return Object.values(ClassMap[char])
    else {
      throw new Error('[CCl byC] Invalid Character ' + char
      + '\nValid Names: ' + Object.keys(ClassMap).join(', '))
    }
  }

  /**
   * @param {string} name name of Game Class to be validated
   * @returns {boolean} if valid name of Game Class
   */
  static isValidByName (name) {
    return Names.includes(name)
  }

  /**
   * @param {string} short shorthand name of Game Class
   * @returns {boolean} if valid shorthand of Game Class
   */
  static isValidByShort (short) {
    return ShortNames.includes(short)
  }
}

export { ClassMap, Names, ShortNames }

export default ElsClasses