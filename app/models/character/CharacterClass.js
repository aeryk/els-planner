import Characters, { AllChars } from './Characters'

class CharacterClass {
  /** @type {string} */
  char

  /** @type {string} */
  name

  /** @type {string} */
  short

  /**
   * @param {Characters|string} char Game Character
   * @param {string} name Game class name
   * @param {string} short Game class shorthand
   */
  constructor (char, name, short) {
    if (char instanceof Characters)
      this.char = char.name.toLowerCase()
    else if (typeof char === 'string' && AllChars[char] != undefined)
      this.char = char
    else
      throw new Error('[CCl Con] Invalid Character ' + char)

    this.name = name
    this.short = short
  }
}

export default CharacterClass
