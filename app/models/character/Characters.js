/** @type {{[name: string]: Characters}} */
const AllChars = {}

class Characters {
  /**
   * @param {string} name Game character name
   * @param {string} wep Name for their weapons
   */
  constructor (name, wep) {
    this.name = name
    this.weaponName = wep
    AllChars[this.name.toLowerCase()] = this
  }

  static Elsword = new Characters('Elsword', 'Sword')
  static Aisha = new Characters('Aisha', 'Staff')
  static Rena = new Characters('Rena', 'Bow')
  static Raven = new Characters('Raven', 'Blade')
  static Eve = new Characters('Eve', 'Drone')
  static Chung = new Characters('Chung', 'Cannon')
  static Ara = new Characters('Ara', 'Spear')
  static Elesis = new Characters('Elesis', 'Claymore')
  static Add = new Characters('Add', 'Dynamo')
  static LuCiel = new Characters('LuCiel', 'Dual Weapon')
  static Rose = new Characters('Rose', 'Guns')
  static Ain = new Characters('Ain', 'Pendulum')
  static Laby = new Characters('Laby', 'Mirror')
  static Noah = new Characters('Noah', 'Sickle')
  static Lithia = new Characters('Lithia', 'Pickaxe')
}

export { AllChars }

export default Characters
