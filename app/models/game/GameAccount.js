import GameCharacter from '@/models/character/GameCharacter'

export default class GameAccount {
  /** @type {string} */
  name

  /** @type {{[ign: string]: GameCharacter}} */
  characters

  /** @type {number} */
  erp

  equipment

  accessories

  sharedTitles

  constructor (name) {
    this.name = name
    this.erp = 0
    this.characters = []
    this.equipment = []
    this.accessories = {}
    this.sharedTitles = {}
  }
}
