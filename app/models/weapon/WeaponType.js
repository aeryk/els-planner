export default class WeaponType {
  constructor (short, long, full) {
    /** @type {string} */
    this.shortName = short

    /** @type {string} */
    this.longName = long

    /** @type {string} */
    this.full = full
  }
}
