import MysticSocket from './MysticSocket'

class MysticSockets {
  /** @type {MysticSocket[]} */
  red = []

  /** @type {MysticSocket[]} */
  blue = []

  /** @type {MysticSocket[]} */
  yellow = []

  /** @type {MysticSocket[]} */
  giant = []
}

export default MysticSockets
