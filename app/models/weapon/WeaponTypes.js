import WeaponType from './WeaponType'

// Dropped SD, Lithia won't have this
// const Secret = new WeaponType('Secret')

// Also dropped, only up to Laby
// const Heroic = new WeaponType('Heroic')

// Assume all weps legendary since the above are dropped

const Void = new WeaponType(
  'ATV',
  'Apcalypse Type - Void',
  'Apcalypse Type - Void %wep%'
)
const Flame = new WeaponType(
  'FoJ',
  'Flames of Judgement',
  'Flames of Judgement - Demonic %wep%'
)
const Vestige = new WeaponType(
  'VoS',
  'Vestige of Soul',
  'Vestige of Soul - %wep% of Requiem'
)
const Submergence = new WeaponType(
  'SoA',
  'Submergence of Abyss',
  'Submergence of Abyss - Phantom %wep%'
)

const WeaponTypes = {
  // Secret,
  // SD: Secret,
  // Heroic,
  // HR: Heroic,
  Void,
  // Add: Void,
  // Flame,
  FoJ: Flame,
  // Vestige,
  VoS: Vestige,
  // Submergence,
  SoA: Submergence
}

export default WeaponTypes
