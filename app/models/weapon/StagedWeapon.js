import Weapon from './Weapon'
import WeaponType from './WeaponType'

class StagedWeapon extends Weapon {
  stage
  progress

  /**
   * @param {WeaponType} wepType Weapon Type
   */
  constructor (wepType) {
    super(wepType)
    if (wepType !== WeaponType.VoS || wepType !== WeaponType.SoA)
      throw new Error('Weapon type ' + wepType.shortName + ' has no stages')

    this.stage = 0
    this.progress = 0
  }
}

export default StagedWeapon
