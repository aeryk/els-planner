import Equipment, { EquipmentSlot } from '@/models/equipment/Equipment'
import MysticSockets from './MysticSockets'
import WeaponType from './WeaponType'
import Characters, { AllChars } from '../character/Characters'
// import WeaponTypes from './WeaponTypes'

export default class Weapon extends Equipment {
  /** @type {EquipmentSlot} */
  type

  /** @type {string} */
  char

  /** @type {string} */
  name

  /** @type {WeaponType} */
  wepType

  /** @type {import('./MysticSockets')} */
  mystics

  // imprints

  // attributes

  constructor (wepType, char) {
    super(EquipmentSlot.Weapon)
    this.rarity = 5 // Legend

    if (char instanceof Characters)
      this.char = char.name.toLowerCase()
    else if (typeof char === 'string' && AllChars[char.toLowerCase()] != undefined)
      this.char = char.toLowerCase()
    else
      throw new Error('[Wep Con] Invalid Character ' + char)

    this.setType(wepType)

    this.sockets = []

    // if void, fill mystics
    this.mystics = new MysticSockets()

    /*
    TODO: Add more defaults:
    
    Empty Imprints
    Empty attributes
    */
  }

  setType (wepType) {
    if (wepType instanceof WeaponType)
      this.wepType = wepType.shortName
    else
      throw new Error('Invalid Weapon Type: ' + wepType)

    this.name = wepType.full.replace('%wep%', AllChars[this.char].weaponName)
  }
}