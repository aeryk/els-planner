import Title from './Title'

export default class Titles {
  static Reaper = new Title('Reaper')
    .setPrerequisites(
      'Achieve Level 60',
      'Clear Heart of Behemoth (5x)'
    )
    .setGoalNames(
      'Demons (Heart of Behemoth)',
      'Dark Trock/Living Trock (Heart of Behemoth)',
      'Final Karis (Heart of Behemoth)'
    )
    .setGoalValues(1500, 500, 25)
    // has implications, but more complicated (1 Karis -> # of demons or trock?)

  static OathOfRuin = new Title('Oath of Ruin')
    .addAlias('oor')
    .setPrerequisites(
      'Achieve Level 70',
      'Clear Grand Cavern: Chasm of the Divided Land (1x)'
    )
    .setGoalNames('Clear Grand Cavern: Chasm of the Divided Land 100 times')
    .setGoalValues(100)
  static OoR = this.OathOfRuin

  static RulesOfTheJungle = new Title('The Rules of the Jungle')
    .addAlias('jungle')
    .setPrerequisites(
      'Achieve Level 80',
      'Clear Diceon Mines (1x)'
    )
    .setGoalNames('Clear Diceon Mines')
    .setGoalValues(10)

  // TODO: Eclipse prereqs

  static Eclipse = new Title('Eclipse')
    .setPrerequisites(
      'Achieve Level 85',
      'Obtain Man Against the Sun',
      'Obtain Shadow\'s Descent',
      'Obtain Resistant to Destiny'
    )
    .setGoalNames('Defeat Solace within 60 secs after entering Phase 2, 10 times (Solace Fortress)')
    .setGoalValues(10)
  
  static GuardianOfElrianode = new Title('Guardian of Elrianode')
    .addAlias('goe')
    .setPrerequisites('Clear El Tower Defense 1 time')
    .setGoalNames('Clear El Tower Defense [Normal]')
    .setGoalValues(150)
  static GoE = this.GuardianOfElrianode

  static ForginaysFruit = new Title('Forginay\'s Fruit')
    .addAlias('forginay')
    .setPrerequisites('Defeat\'Forginay\' (Guardian\'s Forest) (2x)')
    .setGoalNames('Defeat\'Forginay\' (Guardian\'s Forest)')
    .setGoalValues(50)

  static SurvivalOfTheCold = new Title('Survival of the Cold')
    .addAlias('sotc')
    .setPrerequisites('Clear Abandoned Icerite Plant (1x)')
    .setGoalNames('Defeat Monsters from Abandoned Icerite Plant')
    .setGoalValues(13000)
  static SotC = this.SurvivalOfTheCold

  // TODO: Master Road crests

  // Festival

  // Husk of Shadow

  static FreedShadow = new Title('Freed Shadow')
    .setPrerequisites('Obtain Husk of Shadow')
    .setGoalNames(
      'Destroy Severed Shadow Aren\'s Helmet',
      'Shadow Earl\'s Castle 400 times'
    )
    .setGoalValues(400, 400)
    .setImplies([ 0, 1 ])
  static FS = this.FreedShadow

  /* ===== PER-CHARACTER TITLES START ===== */

  // GoSCS

  // SoS

  // Usurper

  // Dark Gaze

  // TSS

  static Versatility = new Title('Versatility')
    .setPrerequisites('Clear Savage White Ghost\'s Castle 1 time (Excludes Story Mode)')
    .setGoalNames('Clear Savage White Ghost\'s Castle 100 times (Excludes Story Mode)')
    .setGoalValues(100)

  static NightParade = new Title('Night Parade of the White-Ghost')
    .setPrerequisites('Clear Altar of Invocation 1 time (Excludes Story Mode)')
    .setGoalNames('Clear Altar of Invocation 100 times (Excludes Story Mode)')
    .setGoalValues(100)
  static NP = this.NightParade

  static BlackAndWhite = new Title('Black and White')
    .setPrerequisites(
      'Obtain Versatility',
      'Obtain Night Parade of the White-Ghost'
    )
    .setGoalNames(
      'Clear Savage White Ghost\'s Castle while still having 1 resurrection 150 times',
      'Clear Altar of Invocation while still having 1 resurrection 150 times'
    )
    .setGoalValues(150, 150)
  static BnW = this.BlackAndWhite
}
