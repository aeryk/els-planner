export default class Title {
  /** @type {string} */
  name

  /** @type {string[]} */
  alias

  /** @type {number[]} */
  goalValues

  /** @type {string[]} */
  goalNames

  /** @type {string[]} */
  prerequisite

  /** @type {number[][]} */
  implies

  /*
  Add implies map
  key: index number
  value: array of other indexes
  TODO: Optional implication multiplier (1 prog on A -> 2 prog on B)
  purpose: when index is incremented, the other indexes in the value are also incremented
  */

  constructor (name) {
    this.name = name
    this.alias = []
    this.goalValues = []
    this.goalNames = []
    this.prerequisite = []
  }

  addAlias (alias) {
    this.alias.push(alias)
    return this
  }

  setGoalValues (...goalValues) {
    this.goalValues = goalValues
    return this
  }

  setGoalNames (...goalNames) {
    this.goalNames = goalNames
    return this
  }

  setPrerequisites (...prereqs) {
    this.prerequisite = prereqs
    return this
  }

  /**
   * Sets progression implications
   * Only works 1:1 for now
   * @param  {...[number, number|number[]]} implies Implication map
   * @returns {Title} The same object
   */
  setImplies (...implies) {
    implies.forEach(v => {
      if (typeof v[1] === 'number')
        this.implies[v[0]] = [v[1]]
      else
        this.implies[v[0]] = v[1]
    })
    return this
  }
}
