import Title from './Title'

export default class TitleProgress {
  /** @type {Title} */
  title

  /** @type {number[]} */
  prereqProgress

  /** @type {number[]} */
  progress

  /**
   * @param {Title} title Title to track
   */
  constructor (title) {
    this.title = title
    this.prereqProgress = Array(title.goalNames.length).fill(0)
    this.progress = Array(title.goalNames.length).fill(0)
  }

  getProgressText () {
    return this.progress.map((p, i) =>
      `${this.title.goalNames[i]} (` +
        p >= this.title.goalValues
        ? '✓)'
        : `(${p}/${this.title.goalValues[i]})`
    )
  }

  incrementProgress (i) {
    this.progress[i]++
    this.title.implies[i]?.forEach((v) => this.progress[v]++)
  }

  incrementProgressDouble (i) {
    this.progress[i] += 2
    this.title.implies[i]?.forEach((v) => this.progress[v] += 2)
  }
}