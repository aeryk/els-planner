export default class Statistic {
  /** @type {string} */
  name

  /** @type {number} */
  max

  /** @type {string} */
  unit

  /**
   * @param {string} name Name
   * @param {number} [max] Max value
   * @param {string} [unit] Unit label
   */
  constructor (name, max, unit) {
    this.name = name
    this.max = max ?? -1 // ie. N/A
    this.unit = unit ?? ''
  }
}
