import { useState } from 'react'

import styles from './SpanInput.module.css'

const SpanInput = ({ children, ...rest }) => {
  const [ value, setValue ] = useState(children)
  const [ editing, setEditing ] = useState(false)

  const onEdit = () => setEditing(true)

  const onSave = (e) => {
    setEditing(false)
    rest.onChange(e)
  }

  const onKeyDown = e => {
    if (e.key === 'Enter') onSave(e)
  }

  const handleChange = e => setValue(e.target.value)

  return editing ? (
    <input
      autoFocus
      {...rest}
      className={styles['input']}
      value={value}
      onChange={handleChange}
      onBlur={onSave}
      onKeyDown={onKeyDown} />
  ) : (
    <span onClick={onEdit}>{value}</span>
  )
}

export default SpanInput

