import Container from 'react-bootstrap/Container'

export default function PageBase ({ children }) {
  return (
    <Container fluid className='my-3'>
      {children}
    </Container>
  )
}