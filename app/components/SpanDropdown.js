
import { useState } from 'react'

export default function SpanDropdown ({
  options,
  optionValue,
  optionLabel,
  initialIndex,
  ...rest
}) {
  const [ editing, setEditing ] = useState(false)
  const [ selected, setSelected ] = useState(initialIndex)

  const onEdit = () => setEditing(true)

  const onSave = (e) => {
    setEditing(false)
    setSelected(e.target.selectedIndex)
    rest.onChange(e)
  }

  const getLabel = (option) => {
    if (typeof optionLabel === 'string')
      return option[optionLabel]
    else
      return optionLabel(option)
  }

  return editing ? (
    <select value={options[selected][optionValue]}
      onChange={onSave} onBlur={onSave}
    >
      {options.map(option => (
        <option
          key={option[optionValue].toLowerCase()}
          value={option[optionValue]}
        >
          {getLabel(option)}
        </option>
      ))}
    </select>
  ) : (
    <span onClick={onEdit} {...rest}>
      {getLabel(options[selected])}
    </span>
  )
}

