import Link from 'next/link'
import Nav from 'react-bootstrap/Nav'

export default function RootNav () {
  return (
    <Nav defaultActiveKey='/' className='bg-body-tertiary' as='nav'>
      <Nav.Item>
        <Link className='nav-link' href='/'>
          Characters
        </Link>
      </Nav.Item>
      <Nav.Item>
        <Link className='nav-link' href='/weapons'>
          Weapons
        </Link>
      </Nav.Item>
      <Nav.Item>
        <Link className='nav-link' href='/sandbox'>
          Sandbox
        </Link>
      </Nav.Item>
    </Nav>
  )
}
