import Image from 'next/image'
import { useState } from 'react'

import CharacterDashboardModal from './modals/CharacterDashboardModal'

import styles from './CharacterListEntry.module.css'
import { CharContext } from './modals/char/CharContext'
import ClassIcon from './ClassIcon'

function getElMaster (char) {
  return {
    'elsword': 'solace',
    'aisha': 'denif',
    'rena': 'ventus',
    'raven': 'solace',
    'eve': 'adrian',
    'chung': 'denif',
    'ara': 'gaia',
    'elesis': 'rosso',
    'add': 'adrian',
    'luciel': 'rosso',
    'rose': 'ventus',
    'ain': 'hernia',
    'laby': 'gaia',
    'noah': 'hernia',
    'lithia': 'gaia'
  }[char]
}

function AdvancementIcon ({ char: { level, char } }) {
  if (level < 70)
    return <span className='ms-2'>Lv.{char.level}</span>
  else if (level < 99) {
    return (
      <Image
        src={'/img/char/t.png'}
        className={'ms-2 ' + styles['a']}
        width={20.5} height={19.5} alt='T' />
    )
  } else {
    return (
      <Image
        src={`/img/char/_mc/${getElMaster(char.toLowerCase())}.png`}
        className={'ms-2 ' + styles['a']}
        width={21.5} height={27} alt='MC' />
    )
  }
}

function CharacterListEntry ({ char, editCharacter, removeCharacter }) {
  const [ showCharDashModal, setShowCharDashModal ] = useState(false)
  const handleShowCharDashModal = () => setShowCharDashModal(true)
  const handleCloseCharDashModal = () => setShowCharDashModal(false)

  const handleDeleteChar = () => {
    removeCharacter(char)
    handleCloseCharDashModal()
  }
  /*
  TODO
  Edit - Display character progression modal
  Delete - prompt for 'are you sure? it will delete everything etc.'

  OR

  Make entire element clickable to open Edit
  Then add delete inside, again with prompt
  */

  return (<>
    <div className={'mt-2 ms-2 pe-2 rounded-end-3 ' + styles['root']}
      onClick={handleShowCharDashModal}
    >
      <ClassIcon char={char} />
      <AdvancementIcon char={char} />
      <span className={'ms-2 d-inline-block align-middle ' + styles['ign']}>{char.ign}</span>
      
    </div>
    <CharContext.Provider value={char}>
      <CharacterDashboardModal show={showCharDashModal} char={char}
        handleClose={handleCloseCharDashModal}
        editCharacter={editCharacter}
        handleDelete={handleDeleteChar} />
    </CharContext.Provider>
  </>)
}

export default CharacterListEntry
