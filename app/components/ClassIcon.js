import Image from 'next/image'
import Colors from 'util/Colors'

import styles from './ClassIcon.module.css'

function ClassIcon ({ char }) {
  const charName = char.char
  const color = Colors.Characters[charName]

  return (
    <div className={styles['class-icon']} style={{ backgroundColor: color }}>
      <Image
        src={`/img/char/${charName.toLowerCase()}/${char.eClass}.png`}
        // src='/img/--.png'
        width={54} height={54} alt={char.eClass.toUpperCase()} />
    </div>
  )
}

export default ClassIcon
