import Image from 'next/image'
import { useId, useRef, useState } from 'react'

import Button from 'react-bootstrap/Button'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import InputGroup from 'react-bootstrap/InputGroup'
import Modal from 'react-bootstrap/Modal'
import Row from 'react-bootstrap/Row'

import { AllChars } from '@/models/character/Characters'
import ElsClasses from '@/models/character/ElsClasses'

import style from './AddCharacterModal.module.css'
import WeaponTypes from '@/models/weapon/WeaponTypes'

export default function AddCharacterModal (props) {
  const { show, handleClose, handleSubmit } = props

  // get name in a roundabout way or just use them directly, ugh
  const [ selectedChar, setSelectedChar ] = useState('elsword')
  const [ selectedClass, setSelectedClass ] = useState('ke')

  const lvlRef = useRef(null)
  const classRef = useRef(null)

  const handleSetLvl99 = () => {
    lvlRef.current.value = 99
  }

  const handleSelectChar = (e) => {
    setSelectedChar(AllChars[e.target.value].name.toLowerCase())
    setSelectedClass(ElsClasses.namesByCharacter(e.target.value)[0].short)
    classRef.current.selectedIndex = 0 // Reset class to 1st path
  }

  const handleSelectClass = (e) => {
    setSelectedClass(e.target.value)
  }

  const handleSelectClassImg = (i) => {
    setSelectedClass(ElsClasses.namesByCharacter(selectedChar)[i].short)
    classRef.current.selectedIndex = i
  }

  const ignId = useId()
  const charLvId = useId()
  const charId = useId()
  const eClassId = useId()
  const wepId = useId()
  const wepEnhId = useId()

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title as='h5'>Add Character</Modal.Title>
      </Modal.Header>
      <Form method='post' onSubmit={handleSubmit}>
        <Modal.Body>
          <Row className='align-items-center'>
            <Form.Group as={Col} xs='auto'>
              <Form.Label htmlFor={ignId}>In-game Name</Form.Label>
              <Form.Control className='mb-2 mr-sm-2' id={ignId} name='ign'
                type='text' required minLength={2} maxLength={12} />
            </Form.Group>
            <Form.Group as={Col} xs='4'>
              <Form.Label htmlFor={charLvId}>
                Level
              </Form.Label>
              <InputGroup>
                <Form.Control className='mb-2 mr-sm-2' id={charLvId}
                  name='lvl' ref={lvlRef} defaultValue={1} type='number'
                  min={1} max={99} step={1} required />
                <Button
                  className='mb-2 mr-sm-2'
                  variant='outline-secondary'
                  onClick={handleSetLvl99}
                >
                  99
                </Button>
              </InputGroup>
            </Form.Group>
          </Row>
          <Row className='align-items-center'>
            <Form.Group as={Col} xs='4'>
              <Form.Label htmlFor={charId}>
                Character
              </Form.Label>
              <Form.Select id={charId} name='char' value={selectedChar}
                onChange={handleSelectChar}
              >
                {
                  Object.entries(AllChars).map(([ k, v ], i) => (
                    <option value={k} key={i}>
                      {v.name}
                    </option>
                  ))
                }
              </Form.Select>
            </Form.Group>
            <Form.Group as={Col} xs='5'>
              <Form.Label htmlFor={eClassId}>
                Class
              </Form.Label>
              <Form.Select
                id={eClassId}
                ref={classRef}
                name='char-class'
                required
                onChange={handleSelectClass}
              >
                {
                  ElsClasses.namesByCharacter(selectedChar).map((cc, i) => (
                    <option value={cc.short} key={i}>
                      {cc.name}
                    </option>
                  ))
                }
              </Form.Select>
            </Form.Group>
          </Row>
          <Row className={style['class-select'] + ' mt-4 mb-3'}>
            {
              ElsClasses.namesByCharacter(selectedChar).map((cc, i) => (
                <Col key={cc.short}>
                  <Image
                    src={`/img/char/${cc.char}/${cc.short}.png`}
                    width={54}
                    height={54}
                    alt={cc.name}
                    onClick={() => handleSelectClassImg(i)}
                    className={
                      selectedClass === cc.short ? style['selected'] : ''
                    } />
                </Col>
              ))
            }
          </Row>
          <Row className={style['wep']}>
            <Form.Group as={Col} xs='12'>
              <Form.Label htmlFor={wepId}>First / Main Weapon</Form.Label>
              <InputGroup>
                <InputGroup.Text>+</InputGroup.Text>
                <Form.Control id={wepEnhId} className='col-4' name='wep-enh'
                  defaultValue={9} type='number' min={0} max={13} step={1}
                  required />
                <Form.Select id={wepId} className='col-8' name='wep' required>
                  {
                    Object.entries(WeaponTypes).map(([ k, w ], i) => (
                      <option value={k} key={i}>
                        {w.full.replace(
                          '%wep%',
                          AllChars[selectedChar]?.weaponName ?? 'Weapon'
                        )}
                      </option>
                    ))
                  }
                </Form.Select>
              </InputGroup>
            </Form.Group>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={handleClose}>
            Close
          </Button>
          <Button variant='primary' type='submit'>
            Add
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  )
}