import { useContext, /* useEffect, useRef */ } from 'react'

import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import Tab from 'react-bootstrap/Tab'
import Tabs from 'react-bootstrap/Tabs'

import ClassIcon from '../ClassIcon'
import { CharContext } from './char/CharContext'
import CharModalWeapons from './char/CharModalWeapons'
import CharModalAccessories from './char/CharModalAccessories'
import CharModalTitles from './char/CharModalTitles'

import './CharacterDashboardModal.css'

function Notes () {
  return (
    <ul>
      <li>
        Weapons
        <ul>
          <li>Supporting more than 1: Main / DPS, EXP, IDR, etc.</li>
          <li>Stage Progression (VoS, SoA)</li>
          <li>Sockets</li>
          <li>Mystic Sockets</li>
          <li>Imprint Sockets</li>
        </ul>
      </li>
      <li>Accessories</li>
      <li>
        Titles
        <ul>
          <li>Progression</li>
        </ul>
      </li>
      <li>
        Master Artifact
        <ul>
          <li>Identification &quot;sockets&quot;</li>
          <li>Spirit Stone &quot;sockets&quot; + color</li>
        </ul>
      </li>
      <li>
        Force Skills
        <ul>
          <li>Henir Fragment Tracking</li>
        </ul>
      </li>
      <li>Customization &quot;sockets&quot;</li>
    </ul>
  )
}

function CharacterDashboardModal (props) {
  const {
    show,
    handleClose,
    editCharacter,
    handleDelete
  } = props

  const char = useContext(CharContext)

  const handleEdit = () => {
    // editCharacter(char) // NO, use mutated 'char'
    handleClose()
  }

  return (
    <Modal size='lg' show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <ClassIcon char={char} />
        <h2 className='ms-3'>{char.ign}</h2>
      </Modal.Header>
      <Modal.Body>
        <Tabs defaultActiveKey='weapons' justify>
          <Tab eventKey='weapons' title='Weapons'>
            <CharModalWeapons editCharacter={editCharacter} />
          </Tab>
          <Tab eventKey='accessories' title='Accessories'>
            <CharModalAccessories editCharacter={editCharacter} />
          </Tab>
          <Tab eventKey='titles' title='Titles'>
            <CharModalTitles editCharacter={editCharacter} />
          </Tab>
          <Tab eventKey='artifact' title='Artifact'>
            <p>Start with grayed out artifact equipment icons.</p>
            <p>
              Then, add some kind of selection + dropdown (?)for color and socket
            </p>
          </Tab>
          <Tab eventKey='forceSkills' title='Force Skills'>
            <p>Start with grayed out Force Skills (Elite + Unique)</p>
            <p>Include Rare if exchangeable</p>
            <p>Add Henir Tracker for weeks left until grind is over</p>
            <p>Add checkboxes for forces to track? Or track all regardless?</p>
          </Tab>
        </Tabs>
      </Modal.Body>
      <Modal.Footer className='px-5 justify-content-evenly'>
        <Button variant='primary' onClick={handleEdit}>Close</Button>
        <Button variant='danger' onClick={handleDelete}>Delete</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default CharacterDashboardModal
