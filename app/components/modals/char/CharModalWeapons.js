import { useContext, useEffect, useState } from 'react'
import { CharContext } from './CharContext'
import { Button, Col, ListGroup, Row, Tab } from 'react-bootstrap'
import Weapon from '@/models/weapon/Weapon'
import WeaponTypes from '@/models/weapon/WeaponTypes'
import SpanInput from '@/components/SpanInput'
import SpanDropdown from '@/components/SpanDropdown'
import { AllChars } from '@/models/character/Characters'

function CharModalWeapons ({ editCharacter }) {
  /** @type {import('@/models/character/GameCharacter')} */
  const char = useContext(CharContext)

  const [ weapons, setWeapons ] = useState(char.weapons)

  useEffect(() => {
    setWeapons(char.weapons)
  }, [])

  const saveWeapons = (newWeapons) => {
    setWeapons(newWeapons)
    editCharacter({
      ...char,
      weapons: newWeapons
    })
  }

  const addWeapon = () => {
    // idk what else coul be a suitable default
    const newWep = new Weapon(WeaponTypes.FoJ, char.char)
    newWep.enhancement = 9

    saveWeapons([ ...weapons, newWep ])
  }

  const editWeapon = (i, nw) => {
    const res = weapons.map((cw, wi) => {
      if (wi === i) return nw
      else return cw
    })
    saveWeapons(res)
  }

  const removeWeapon = (i) => {
    const res = [...weapons]
    res.splice(i, 1)
    saveWeapons(res)
  }

  return (
    <Tab.Container defaultActiveKey={weapons.length > 0 ? '#wep-1' : 'none'}>
      <Row className='mt-4'>
        <Col sm={3}>
          <ListGroup>
            {
              weapons.length > 0
                ? weapons.map((w, i) => {
                  const key = `wep-${i+1}`
                  return(
                    <ListGroup.Item action href={'#' + key} key={key}>
                      +{w.enhancement} {w.wepType}
                    </ListGroup.Item>
                  )
                })
                : (
                  <ListGroup.Item action href='none' disabled>
                    No weapons
                  </ListGroup.Item>
                )
            }
            <ListGroup.Item action variant='primary' onClick={addWeapon}>
              Add
            </ListGroup.Item>
          </ListGroup>
        </Col>
        <Col sm='auto'>
          <Tab.Content>
            {
              weapons.length > 0
                ? weapons.map((w, i) => (
                  <Tab.Pane key={w.name} eventKey={`#wep-${i + 1}`}>
                    <Row>
                      <Col md='auto'>
                        <h6>
                          {'+'}
                          <SpanInput
                            type='number'
                            inputMode='numeric'
                            min={0}
                            max={13}
                            step={1}
                            onChange={e => {
                              const nw = { ...w }
                              nw.enhancement = parseInt(e.target.value)
                              editWeapon(i, nw)
                            }}
                          >
                            {w.enhancement}
                          </SpanInput>
                          {' '}
                          {/* {w.name} */}
                          <SpanDropdown
                            options={Object.values(WeaponTypes)}
                            optionValue='shortName'
                            optionLabel={o => o.full.replace(
                              '%wep%',
                              AllChars[char.char]?.weaponName ?? 'Weapon'
                            )}
                            initialIndex={
                              Object.keys(WeaponTypes).indexOf(w.wepType)
                            }
                            onChange={e => {
                              const newType = Object.values(WeaponTypes)[
                                e.target.selectedIndex
                              ]
                              let nw = new Weapon(newType, char.char)
                              nw = Object.assign(nw, w)
                              nw.setType(newType)
                              editWeapon(i, nw)
                            }} />
                        </h6>
                      </Col>
                      <Col xs lg={1}>
                        <Button variant='danger' onClick={() => removeWeapon(i)}>
                          Del
                        </Button>
                      </Col>
                    </Row>
                    
                  </Tab.Pane>
                ))
                : <Tab.Pane eventKey='none'>No weapon added.</Tab.Pane>
            }
          </Tab.Content>
        </Col>
      </Row>
    </Tab.Container>
  )
}
/*
<>
    <p>{JSON.stringify(char)}</p>
    <p>({char.weapons.length}): [{char.weapons.join(' ')}]</p>
    <p>Weapon list as sidebar, with Add button on the bottom</p>
    <p>Rest of the space is filled with info when a wep is clicked</p>
  </>
*/

export default CharModalWeapons