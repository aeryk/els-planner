import Image from 'next/image'
import { Button, ButtonGroup, Card, ListGroup, ProgressBar, Row } from 'react-bootstrap'

function CharModalTitles () {
  return (<>
    <p>Just like Weapons, sidebar first. Maybe small icon before name?</p>
    <p>OR a Card view like so (all hardcoded)</p>
    <Card style={{ width: '45%' }}>
      <Image
        src={'/img/title/black-and-white.png'}
        width={105} height={70} alt='Black and White'
        className='card-img-top' />
      <Card.Body>
        <Card.Title>Black and White</Card.Title>
        <Card.Subtitle>Raid Title, Pruinaum</Card.Subtitle>
      </Card.Body>
      <ListGroup className='list-group-flush'>
        <ListGroup.Item>
          <p>Clear Savage White Ghost&apos;s Castle while still having 1 resurrection 150 times</p>
          <Row className='justify-content-center .progress-counter'>
            <Button variant='primary' size='sm' className='col-1'>-</Button>
            <div className='col-8 px-1'>
              <ProgressBar now={(130/150*100).toFixed(2)} label='130/150' />
            </div>
            <ButtonGroup className='col-2'>
              <Button variant='primary' size='sm'>+</Button>
              <Button variant='primary' size='sm'>+2</Button>
            </ButtonGroup>
          </Row>
        </ListGroup.Item>
        <ListGroup.Item>
          <p>Clear Altar of Invocation while still having 1 resurrection 150 times</p>
          <ProgressBar now={(18/150*100).toFixed(2)} label='18/150' />
        </ListGroup.Item>
      </ListGroup>
    </Card>
    <p>
      Missing: buttons to increment, or a way to directly overwrite progress
      <br />
      Also, too long descriptions :v
    </p>
    <p>
      There&apos;s already some work done on the progression functionality
    </p>
  </>)
}

export default CharModalTitles