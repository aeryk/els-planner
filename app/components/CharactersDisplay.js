import { useState, useEffect } from 'react'
import Button from 'react-bootstrap/Button'

import { AllChars } from '@/models/character/Characters'
import { ClassMap } from '@/models/character/ElsClasses'
import GameCharacter from '@/models/character/GameCharacter'
import AddCharacterModal from './modals/AddCharacterModal'
import CharacterListEntry from './CharacterListEntry'

import styles from './CharactersDisplay.module.css'
import WeaponTypes from '@/models/weapon/WeaponTypes'

function CharacterList ({
  characters,
  filterText,
  editCharacter,
  removeCharacter
}) {
  filterText = filterText.toLowerCase()
  const filtered = characters.filter(v => {
    return (
      // IGN match
      v.ign.toLowerCase().includes(filterText) ||

      // Class name match
      ClassMap[v.char.toLowerCase()][v.eClass].name.toLowerCase().includes(filterText) ||

      // Class abbreviation match
      v.eClass.includes(filterText)
    )
  })

  if (characters.length === 0)
    return (<p>No characters. Add one below.</p>)
  else if (filtered.length === 0)
    return (<p>No matching entries.</p>)
  else {
    return (
      <div className={'pe-2 ' + styles['list']}>
        {filtered.map((c, i) => (
          <CharacterListEntry char={c} key={i}
            editCharacter={(ch) => editCharacter(i, ch)}
            removeCharacter={removeCharacter} />
        ))}
      </div>
    )
  }
}

export default function CharactersDisplay () {
  const [ show, setShow ] = useState(false)
  const [ characters, setCharacters ] = useState(
    /** @type {GameCharacter[]} */ ([])
  )
  const [ charsLoaded, setCharsLoaded ] = useState(false)
  const [ filterText, setFilterText ] = useState('')

  useEffect(() => {
    const rawChars = localStorage.getItem('characters')
    if (!rawChars) {
      const newArr = []
      setCharacters(newArr)
    } else {
      let parsedChars = JSON.parse(rawChars) /* eslint-disable-line prefer-const */
      if (Array.isArray(parsedChars)) {
        // reprocess for new values; comment out if no need
        parsedChars = parsedChars.map(pc => {
          let ngc = new GameCharacter(pc.ign, pc.level, pc.char, pc.eClass)
          ngc = Object.assign(ngc, pc)
          return ngc
        })
        setCharacters(parsedChars)
      }
    }
  }, [])

  useEffect(() => {
    if (charsLoaded)
      localStorage.setItem('characters', JSON.stringify(characters))
    else
      setCharsLoaded(true)
  }, [characters])

  const addCharacter = (e) => {
    e.preventDefault()
    const formData = new FormData(e.target)

    const ign = formData.get('ign')
    const level = formData.get('lvl')
    const echar = AllChars[formData.get('char')]
    const eclass = formData.get('char-class')
    const wep = formData.get('wep')
    const wepEnh = formData.get('wep-enh')

    const newChar = new GameCharacter(ign, level, echar, eclass)
    const charWep = newChar.addWeapon(WeaponTypes[wep])
    charWep.enhancement = wepEnh

    setCharacters([ ...characters, newChar ])
    handleClose()
  }

  const editCharacter = (i, c) => {
    if (characters[i]?.char !== c?.char) {
      throw new Error(
        `[Edit Char] Mismatched Chars: ${characters[i]?.char} and ${c?.char}\n`
        + `IGN ${characters[i]?.ign}`
      )
    }

    const res = characters.map((cc, ci) => {
      if (ci === i) return c
      else return cc
    })
    setCharacters(res)
  }

  const removeCharacter = c => {
    const idx = characters.findIndex(v => c.ign === v.ign)
    if (idx !== -1) {
      const newArr = characters.slice()
      newArr.splice(idx, 1)
      setCharacters(newArr)
    }
  }

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  return (
    <>
      <h1 className='mb-3'>Characters</h1>
      <span className='mx-2'>Filter</span>
      <input onChange={e => setFilterText(e.target.value)} />
      <CharacterList characters={characters} filterText={filterText}
        editCharacter={editCharacter}
        removeCharacter={removeCharacter} />
      <Button className='mt-2' variant='primary' onClick={handleShow}>
        Add
      </Button>

      <AddCharacterModal
        show={show}
        handleClose={handleClose}
        handleSubmit={addCharacter}
      ></AddCharacterModal>
    </>
  )
}
